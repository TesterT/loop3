﻿using System;

namespace LoopTasks
{
    public static class LoopTasks
    {
        
        /// <summary>
        /// Task 1
        /// </summary>
        public static int SumOfOddDigits(int n)
        {
            
            int result = 0;
            /*if (n > 0)
            {*/
                string stringN = n.ToString();
                

                for (int i = 0; i < stringN.Length; i++)
                {
                    if ((int)Char.GetNumericValue(stringN[i]) % 2 != 0)
                    {
                        result += (int)Char.GetNumericValue(stringN[i]);
                        
                    }
                }
            /*}*/
            
            return result;
        }

        /// <summary>
        /// Task 2
        /// </summary>
        public static int NumberOfUnitsInBinaryRecord(int n)
        {
             string binaryN = Convert.ToString(n, 2);

            var result = 0;

            for (int i = 0; i < binaryN.Length; i++)   
            {
                if (binaryN[i].Equals('1'))
               
                {
                    result++;
                }
            }
            Console.WriteLine(result);
            return result;
        }

        /// <summary>
        /// Task 3 
        /// </summary>
        public static int SumOfFirstNFibonacciNumbers(int n)
        {
            int result = 0;

            for (int i = 0; i < n; i++)
            {
                result += Fibonacci(i);
            }

            return result;
        }

        private static int Fibonacci(int n)
        {
            int a = 0;
            int b = 1;
            int tmp;

            for (int i = 0; i < n; i++)
            {
                tmp = a;
                a = b;
                b += tmp;
            }

            return a;
        }
        
    }
}